using System;
using System.Linq;
using System.Threading.Tasks;
using GerenciadorDeUsuarios.Models;
using GerenciadorDeUsuarios.Repositorios;
using Microsoft.AspNetCore.Mvc;

namespace GerenciadorDeUsuarios.Controllers
{
    public class UsuarioController : Controller
    {
        public IUsuario Usuarios { get; }
        public UsuarioController(IUsuario usuarios)
        {
            this.Usuarios = usuarios;

        }
        public async Task<IActionResult> Todos(int totalPorPagina = 5, int pagina = 1)
        {

            var usuarios = await Usuarios.GetAll();
            
            var quantPaginas = Math.Ceiling(usuarios.Count/Convert.ToDouble(totalPorPagina));
            
            
            var usuariosPaginado = usuarios
                                        .Skip(totalPorPagina * (pagina - 1))
                                        .Take(totalPorPagina)
                                        .ToList();

            ViewData["PaginaAnterior"] = pagina > 1 ? pagina - 1 : 1;
            ViewData["ProximaPagina"] = pagina == quantPaginas ? pagina : pagina + 1;
            var viewModel = new UsuarioViewModel(usuariosPaginado,(int)quantPaginas);
            return View(viewModel);
        }

        private IActionResult NewMethod(System.Collections.Generic.List<Usuario> usuariosPaginado)
        {
            return View(usuariosPaginado);
        }

        public IActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Novo([FromForm]Usuario usuario)
        {
            if(ModelState.IsValid)
            {
              await Usuarios.Save(usuario);
              return RedirectToAction("Todos");  
            }
           
            return View();      
                
            
        }


        public async Task<IActionResult> Editar(Guid id)
        {
            var usuario = await Usuarios.GetById(id);
            return View(usuario);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Editar([FromForm]Usuario usuario, Guid id)
        {
            usuario.UsuarioId = id;
            await Usuarios.Update(usuario);
            return RedirectToAction("Todos");
        }

        public async Task<IActionResult> Deletar(Guid id)
        {
            var usuario = await Usuarios.GetById(id);
            return View(usuario);
        }

        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmacaoDeletar(Guid id)
        {
            
            await Usuarios.Delete(id);
            return RedirectToAction("Todos");
        }
        
       
        public IActionResult Permissoes(Guid id)
        {
            var usuario = Usuarios.GetUserWith(id);
            return View(usuario);
        } 


       
        

    }
}