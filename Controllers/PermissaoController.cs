using System;
using System.Threading.Tasks;
using GerenciadorDeUsuarios.Models;
using GerenciadorDeUsuarios.Repositorios;
using Microsoft.AspNetCore.Mvc;

namespace GerenciadorDeUsuarios.Controllers
{
    public class PermissaoController : Controller
    {
        public IRepositorioBase<Permissao> Permissoes { get; }
        public PermissaoController(IRepositorioBase<Permissao> permissoes)
        {
            this.Permissoes = permissoes;

        }
        public async Task<IActionResult> Todos()
        {
            var permissoes = await Permissoes.GetAll();
            return View(permissoes);
        }


        public IActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Novo([FromForm]Permissao Permissao)
        {
            if(ModelState.IsValid)
            {
              await Permissoes.Save(Permissao);
              return RedirectToAction("Todos");  
            }           
            return View();                 
        }


        public async Task<IActionResult> Editar(Guid id)
        {
            var permissao = await Permissoes.GetById(id);
            return View(permissao);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Editar([FromForm]Permissao Permissao, Guid id)
        {
            Permissao.PermissaoId = id;
            await Permissoes.Update(Permissao);
            return RedirectToAction("Todos");
        }

        public async Task<IActionResult> Deletar(Guid id)
        {
            var permissao = await Permissoes.GetById(id);
            return View(permissao);
        }

        [HttpPost, ActionName("Deletar")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmacaoDeletar(Guid id)
        {
            
            await Permissoes.Delete(id);
            return RedirectToAction("Todos");
        }

      
    }
}