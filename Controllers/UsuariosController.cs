using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GerenciadorDeUsuarios.Models;
using GerenciadorDeUsuarios.Repositorios;
using Microsoft.AspNetCore.Mvc;

namespace GerenciadorDeUsuarios.Controllers
{
    [Route("api/[controller]")]
    public class UsuariosController : Controller
    {
        public IUsuario Usuarios { get; }
        public UsuariosController(IUsuario usuarios)
        {
            this.Usuarios = usuarios;

        }
        public async Task<IList<Usuario>> Get()
        {
            return await Usuarios.GetAll();            
        }

        [HttpGet("{id}")]
        public async Task<Usuario> Get(Guid id)
        {
            return await Usuarios.GetById(id);
        }

        [HttpPost]
        public async Task Post([FromBody]Usuario usuario)
        {
            await Usuarios.Save(usuario);
        }

        [HttpPut("{id}")]
        public async Task Put([FromBody]Usuario usuario, Guid id)
        {
            usuario.UsuarioId = id;
            await Usuarios.Update(usuario);
        }
        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {            
            await Usuarios.Delete(id);
        }
    }
}