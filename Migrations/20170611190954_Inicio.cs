﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GerenciadorDeUsuarios.Migrations
{
    public partial class Inicio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tb_Permissoes",
                columns: table => new
                {
                    PermissaoId = table.Column<Guid>(nullable: false),
                    NomePermissao = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_Permissoes", x => x.PermissaoId);
                });

            migrationBuilder.CreateTable(
                name: "tb_usuarios",
                columns: table => new
                {
                    IdUsuario = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(maxLength: 255, nullable: false),
                    Nome = table.Column<string>(maxLength: 100, nullable: false),
                    Senha = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_usuarios", x => x.IdUsuario);
                });

            migrationBuilder.CreateTable(
                name: "tb_PermissoesUsuario",
                columns: table => new
                {
                    UsuarioId = table.Column<Guid>(nullable: false),
                    PermissaoId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_PermissoesUsuario", x => new { x.UsuarioId, x.PermissaoId });
                    table.ForeignKey(
                        name: "FK_tb_PermissoesUsuario_tb_Permissoes_PermissaoId",
                        column: x => x.PermissaoId,
                        principalTable: "tb_Permissoes",
                        principalColumn: "PermissaoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_tb_PermissoesUsuario_tb_usuarios_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "tb_usuarios",
                        principalColumn: "IdUsuario",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tb_PermissoesUsuario_PermissaoId",
                table: "tb_PermissoesUsuario",
                column: "PermissaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tb_PermissoesUsuario");

            migrationBuilder.DropTable(
                name: "tb_Permissoes");

            migrationBuilder.DropTable(
                name: "tb_usuarios");
        }
    }
}
