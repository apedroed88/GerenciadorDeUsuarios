using System;
using GerenciadorDeUsuarios.Models;

namespace GerenciadorDeUsuarios.Repositorios
{
    public interface IUsuario : IRepositorioBase<Usuario>
    {
         Usuario GetUserWith(Guid id);
         
    }
}