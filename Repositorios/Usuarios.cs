using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using GerenciadorDeUsuarios.DbContexts;
using GerenciadorDeUsuarios.Models;
using GerenciadorDeUsuarios.Seguranca;
using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.Repositorios
{
    public class Usuarios : IUsuario
    {
        public async Task Delete(Guid id)
        {
            using (var db = new GerenciadorDeUsuariosContext())
            {
                var usuario = await db.Usuarios.FindAsync(id);
                db.Usuarios.Remove(usuario);
                await db.SaveChangesAsync();
            }
        }

        public async Task<IList<Usuario>> GetAll()
        {
            using (var db = new GerenciadorDeUsuariosContext())
            {
                return await db.Usuarios.ToListAsync();
            }
        }

        public async Task<Usuario> GetById(Guid id)
        {
            using (var db = new GerenciadorDeUsuariosContext())
            {
                return await db.Usuarios.FindAsync(id);
            }
        }

        public async Task<Usuario> GetUserWith(Guid id)
        {
            using (var db = new GerenciadorDeUsuariosContext())
            {
                return await db.Usuarios.Include(x => x.PermissoesUsuario).Where(x => x.UsuarioId == id).FirstOrDefaultAsync();
            }
        }

        public async Task Save(Usuario obj)
        {
            using (var db = new GerenciadorDeUsuariosContext())
            {
                // Criptografando senha antes de salvar no banco.
                var hash = new Hash(SHA512.Create());
                obj.Senha = hash.CriptografarSenha(obj.Senha);

                await db.Usuarios.AddAsync(obj);
                await db.SaveChangesAsync();
            }

        }

        public async Task Update(Usuario obj)
        {
            using (var db = new GerenciadorDeUsuariosContext())
            {
                // Criptografando senha antes de salvar no banco.
                var hash = new Hash(SHA512.Create());
                obj.Senha = hash.CriptografarSenha(obj.Senha);
                db.Usuarios.Update(obj);
                await db.SaveChangesAsync();
            }
        }

        Usuario IUsuario.GetUserWith(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}