using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GerenciadorDeUsuarios.DbContexts;
using GerenciadorDeUsuarios.Models;
using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.Repositorios
{
    public class Permissoes : IRepositorioBase<Permissao>
    {
       
        public async Task Delete(Guid id)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                var permissao = await db.Permissoes.FindAsync(id);
                db.Permissoes.Remove(permissao);
                await db.SaveChangesAsync();
                
            }
        }

        public async Task<IList<Permissao>> GetAll()
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
               return await db.Permissoes.ToListAsync();                
            }
        }

        public async Task<Permissao> GetById(Guid id)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                return await db.Permissoes.FindAsync(id);
            }
        }

        public async Task Save(Permissao obj)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                await db.Permissoes.AddAsync(obj);
                await db.SaveChangesAsync();
            }
            
        }

        public async Task Update(Permissao obj)
        {
            using(var db = new GerenciadorDeUsuariosContext())
            {
                
                 db.Permissoes.Update(obj);
                 await db.SaveChangesAsync();
            }
        }
    }
}