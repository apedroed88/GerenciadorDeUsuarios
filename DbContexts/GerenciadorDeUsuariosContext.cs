using GerenciadorDeUsuarios.Maps;
using GerenciadorDeUsuarios.Models;
using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.DbContexts
{
    public class GerenciadorDeUsuariosContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Permissao> Permissoes { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlite("Data Source=App.db");
        }
        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            new UsuarioMap().Mapear(modelbuilder);
            new PermissaoMap().Mapear(modelbuilder);
            new PermissoesUsuarioMap().Mapear(modelbuilder);
        }

    }
}