using System;
using System.Security.Cryptography;
using System.Text;

namespace GerenciadorDeUsuarios.Seguranca
{
    public class Hash
    {
        public HashAlgorithm Algoritimo { get; }
        private string Chave { get; }
        public Hash(HashAlgorithm algoritimo)
        {
            this.Algoritimo = algoritimo;
            this.Chave = "chaveDeSegurança";
        }

        public string CriptografarSenha(string senha)
        {
            
            var bytes = Encoding.UTF8.GetBytes(senha+this.Chave);
            var senhaEncriptada = this.Algoritimo.ComputeHash(bytes);
            var stringBuilder = new StringBuilder();
            foreach (var item in senhaEncriptada)
                stringBuilder.Append(item.ToString("X2"));
            
            return stringBuilder.ToString();            
        }
        public bool ValidarSenha(string senhaDigitada, string senhaArmazenada)
        {
            if(string.IsNullOrEmpty(senhaArmazenada))
                throw new NullReferenceException("Senha não cadastrada.");
            var senhaDigitadaEncriptada = this.CriptografarSenha(senhaDigitada);
            return senhaDigitadaEncriptada == senhaArmazenada;        
        }
    }
}