using System;
using System.Collections.Generic;

namespace GerenciadorDeUsuarios.Models
{
    public class Permissao
    {
        public Guid PermissaoId { get; set; }        
        public string Nome { get; set; }
        public IList<PermissoesUsuario> PermissoesUsuario { get; set; }

    }
}