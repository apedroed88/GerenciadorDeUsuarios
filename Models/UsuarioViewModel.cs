using System.Collections.Generic;

namespace GerenciadorDeUsuarios.Models
{
    public class UsuarioViewModel
    {
        public IList<Usuario> Usuarios { get; set; }
        public int QuantPaginas { get; set; }
        
        public UsuarioViewModel(IList<Usuario> usuarios, int quantidadeDePaginas)
        {
            this.Usuarios = usuarios;
            this.QuantPaginas = quantidadeDePaginas;
        }
        public UsuarioViewModel()
        {
            
        }
    }
}