using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GerenciadorDeUsuarios.Models
{
    public class Usuario
    {
        public Guid UsuarioId { get; set; }

        [Required(ErrorMessage = "Esse campo não pode estar vazio!")]
        [StringLength(100, ErrorMessage = "Tamanho maximo de 100 caracteres" )]
        public string Nome { get; set; }
        
        [Required(ErrorMessage = "Esse campo não pode estar vazio!")]
        [StringLength(255, ErrorMessage = "Tamanho maximo de 255 caracteres!")]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Esse campo não pode estar vazio!")]
        [StringLength(10, MinimumLength = 3, ErrorMessage="Quantidade de caracteres invalida")]
        public string Senha { get; set; }
        public IList<PermissoesUsuario> PermissoesUsuario {get;set;} = new List<PermissoesUsuario>();

    }
}