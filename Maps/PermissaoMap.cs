using System;
using GerenciadorDeUsuarios.Models;
using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.Maps
{
    public class PermissaoMap : IClassMap
    {
        public void Mapear(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Permissao>(p =>
            {
                p.ToTable("tb_Permissoes");
                p.HasKey(k => k.PermissaoId);
                p.Property(x => x.PermissaoId).ValueGeneratedOnAdd();
                p.Property(x => x.Nome).HasColumnName("NomePermissao").HasMaxLength(100).IsRequired();
            });
        }
    }
}