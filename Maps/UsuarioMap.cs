using System;
using GerenciadorDeUsuarios.Models;
using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.Maps
{
    public class UsuarioMap : IClassMap
    {
        public void Mapear(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>( u => 
            {
               u.ToTable("tb_usuarios");
               u.HasKey(k => k.UsuarioId);
               u.Property(p => p.UsuarioId).HasColumnName("IdUsuario").ValueGeneratedOnAdd();
               u.Property(p => p.Nome).HasMaxLength(100).IsRequired();
               u.Property(p => p.Email).HasMaxLength(255).IsRequired();
               u.Property(p => p.Senha).HasMaxLength(10).IsRequired();
            });
        }
    }
}