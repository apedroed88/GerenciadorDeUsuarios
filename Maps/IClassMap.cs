using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.Maps
{
    public interface IClassMap
    {
         void Mapear(ModelBuilder modelBuilder);
    }
}