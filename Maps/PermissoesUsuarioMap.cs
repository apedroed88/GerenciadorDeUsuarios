using System;
using GerenciadorDeUsuarios.Models;
using Microsoft.EntityFrameworkCore;

namespace GerenciadorDeUsuarios.Maps
{
    public class PermissoesUsuarioMap : IClassMap
    {
        public void Mapear(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PermissoesUsuario>(e =>
            {
                e.ToTable("tb_PermissoesUsuario");
                e.HasKey(k => new {k.UsuarioId, k.PermissaoId});
                e.HasOne(x => x.Usuario).WithMany(x => x.PermissoesUsuario).HasForeignKey(x => x.UsuarioId);
                e.HasOne(x => x.Permissao).WithMany(x => x.PermissoesUsuario).HasForeignKey(x => x.PermissaoId);
            });
        }
    }
}